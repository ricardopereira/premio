# Number Assignment App

This application is a simple web-based tool to assign unique numbers to names. It's built with Node.js and Express and uses a local JSON file for data storage. The frontend is a simple HTML page with JavaScript to interact with the backend API.

## Features

- Assign a unique number to a person
- View all current number assignments
- Delete an individual assignment
- Reset all assignments

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to have Node.js and npm installed on your machine. If you don't have them installed, follow the instructions [here](https://nodejs.org/en/download/) to install them.

### Installing

First, clone the repository to your local machine:

```bash
git clone <git-repo>/number-assignment-app.git
```

Then, navigate to the cloned repository and install the dependencies:

```bash
cd number-assignment-app
npm install
```

### Running the Application

To start the application, run the following command:

```bash
npm start
```

The application will be available at `http://localhost:3000` in your web browser.

## Built With

- [Node.js](https://nodejs.org/) - The runtime server environment
- [Express](https://expressjs.com/) - The web framework used