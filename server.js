const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;

const { Pool } = require('pg');
const pool = new Pool({
  connectionString: process.env.POSTGRES_URL + "?sslmode=require",
})

app.use(bodyParser.json());
app.use(express.static('public'));

// API endpoints
app.get('/assignments', async (req, res) => {
  try {
    const { rows } = await pool.query('SELECT * FROM assignments');
    res.json(rows);
  } catch (err) {
    res.status(500).send(`Server error: ${err.message}`);
  }
});

app.post('/assign', async (req, res) => {
  const { name, number } = req.body;
  try {
    await pool.query('INSERT INTO assignments (number, name) VALUES ($1, $2)', [number, name]);
    res.send(`Número ${number} adicionada com sucesso a ${name}.`);
  } catch (err) {
    res.status(500).send(`Server error: ${err.message}`);
  }
});

app.post('/reset', async (req, res) => {
  try {
    await pool.query('DELETE FROM assignments');
    res.send('Todas as atribuições foram eliminadas.');
  } catch (err) {
    res.status(500).send(`Server error: ${err.message}`);
  }
});

app.delete('/unassign/:number', async (req, res) => {
  const { number } = req.params;
  try {
    const result = await pool.query('DELETE FROM assignments WHERE number = $1', [number]);
    if (result.rowCount > 0) {
      res.send('Atribuição eliminada com sucesso.');
    } else {
      res.status(404).send('Número não encontrado.');
    }
  } catch (err) {
    res.status(500).send(`Server error: ${err.message}`);
  }
});

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});
