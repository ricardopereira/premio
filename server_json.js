const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(express.static('public'));

// Load or initialize the JSON file
let assignments = {};
const dataPath = './assignments.json';

try {
  const fileContents = fs.readFileSync(dataPath, 'utf8');
  assignments = JSON.parse(fileContents);
} catch (error) {
  console.log('No existing assignments. Starting fresh.');
  fs.writeFileSync(dataPath, JSON.stringify(assignments), 'utf8');
}

// API endpoints
app.get('/assignments', (req, res) => {
  res.json(assignments);
});

app.post('/assign', (req, res) => {
  const { name, number } = req.body;
  if (assignments[number]) {
    return res.status(409).send('Number already assigned.');
  }
  assignments[number] = name;
  fs.writeFileSync(dataPath, JSON.stringify(assignments), 'utf8');
  res.send('Número associado com sucesso.');
});

app.post('/reset', (req, res) => {
  assignments = {};
  fs.writeFileSync(dataPath, JSON.stringify(assignments), 'utf8');
  res.send('Todas as atribuições foram eliminadas.');
});

app.delete('/unassign/:number', (req, res) => {
  const { number } = req.params;
  if (assignments[number]) {
    delete assignments[number];
    fs.writeFileSync(dataPath, JSON.stringify(assignments), 'utf8');
    res.send('Atribuição eliminada com sucesso.');
  } else {
    res.status(404).send('Número não encontrado.');
  }
});

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});
